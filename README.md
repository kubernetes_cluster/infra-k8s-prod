# Terragrunt IAC EKS-Cluster with AWS Load Balancer Controller, ArgoCD + RDS | + Git pre-commit hook | + Terraform/Terragrunt CI/CD Pipeline | prod-enviroment

## Terraform
```sh
Version ~> 1.6.6
```


## Terragrunt
```sh
Version ~> 0.54.5
```



## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 5.0 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | ~> 2.24.0 |
| <a name="provider_tls"></a> [tls](#provider\_tls) | ~> 2.24.0 |
| <a name="provider_helm"></a> [helm](#provider\_helm) | ~> 4.0.5 |
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | ~> 16.7.0 |
| <a name="provider_kubectl"></a> [kubectl](#provider\_kubectl) | ~> 1.14 |






## Owner
```sh
Volodymyr Hrytsayenko
```